﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Houses.ForSale.Models;

namespace Houses.ForSale.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            AgentProfileModel agentProfileModel = new AgentProfileModel();
            agentProfileModel.agentOverview = GetAgentOverview();
            agentProfileModel.lstAgentActiveListing = GetActiveListing();
            agentProfileModel.lstAgentPastSale = GetAgentPastSale();
            agentProfileModel.lstAgentReview = GetAgentReview();

            return View(agentProfileModel);
        }

        private AgentOverviewModel GetAgentOverview()
        {
            AgentOverviewModel agentOverview = new AgentOverviewModel
            {
                AgentId = 1,
                AgentFirstName = "Melissa",
                AgentLastName = "Crosby",
                AgentPhoto = "agent5.png",
                AgentRating = (decimal)1.5,
                AgentReviews = 20,
                LocalKnowledgeRating = (decimal)3.6,
                ProcessExpertiseRating = (decimal)2.4,
                ResponsivenessRating = (decimal)4.2,
                NegotiationSkillsRating = (decimal)4.6,
                AgentDescription = "Being a full-service Realtor since 2007, I have been baptized by fire in a very tough housing market. I have successfully closed over 60 transactions and processed over 70 short sales both as the listing agent and some for other agents. I am very knowledgeable about lenders and their processes. I strive to exceed expectations and never forget that I am always accountable to my clients.",
                AgentObjective = "My objective is to establish relationships for life, not just for the current transaction. I enjoy assisting home buyers and sellers to get moved to a better place, one that is exciting.",
                AgentBrokerage = "Berkshire Hathaway HomeServices Elite Real Estate",
                AgentSpecialties = "Property Management, Buyer’s Agent, Listing Agent…",
                AgentLicenseNumber = "#5452129"
            };

            return agentOverview;
        }

        private List<AgentActiveListingModel> GetActiveListing()
        {
            List<AgentActiveListingModel> lstAgentActiveListing = new List<AgentActiveListingModel>();

            AgentActiveListingModel agentActiveListingModel1 = new AgentActiveListingModel
            {
                ListingId = 1,
                IsFavorite = true,
                PropertyImage = "home1.jpg",
                PropertyPrice = 345000,
                IsInstallment = true,
                InstallmentPrice = 65,
                PropertyAddress = "472 Test Address, City",
                TotalBeds = 3,
                TotalBath = 4,
                TotalSquareFeet = 2788
            };
            lstAgentActiveListing.Add(agentActiveListingModel1);

            AgentActiveListingModel agentActiveListingModel2 = new AgentActiveListingModel
            {
                ListingId = 2,
                IsFavorite = false,
                PropertyImage = "home8.jpg",
                PropertyPrice = 625800,
                IsInstallment = false,
                InstallmentPrice = 12,
                PropertyAddress = "50 Test Address 2, City",
                TotalBeds = 2,
                TotalBath = 2,
                TotalSquareFeet = 5788
            };
            lstAgentActiveListing.Add(agentActiveListingModel2);

            AgentActiveListingModel agentActiveListingModel3 = new AgentActiveListingModel
            {
                ListingId = 3,
                IsFavorite = false,
                PropertyImage = "home11.jpg",
                PropertyPrice = 665000,
                IsInstallment = null,
                InstallmentPrice = null,
                PropertyAddress = "22 Test Address 3, City",
                TotalBeds = 4,
                TotalBath = 4,
                TotalSquareFeet = 2288
            };
            lstAgentActiveListing.Add(agentActiveListingModel3);

            return lstAgentActiveListing;
        }

        private List<AgentPastSaleModel> GetAgentPastSale()
        {
            List<AgentPastSaleModel> lstAgentPastSale = new List<AgentPastSaleModel>();

            AgentPastSaleModel agentPastSaleModel1 = new AgentPastSaleModel
            {
                SaleId = 1,
                PropertyImage = "home5.jpg",
                PropertyPrice = 345000,
                PropertyAddress = "3247 Greystone Dr",
                City = "Jamul",
                State = "CA",
                Postcode = "91935",
                TotalBeds = 3,
                TotalBath = 4,
                TotalSquareFeet = 2788,
                Represented = "Seller",
                SoldDate = new DateTime(2016, 2, 3)
            };
            lstAgentPastSale.Add(agentPastSaleModel1);

            AgentPastSaleModel agentPastSaleModel2 = new AgentPastSaleModel
            {
                SaleId = 2,
                PropertyImage = "home4.jpg",
                PropertyPrice = 490000,
                PropertyAddress = "9951 Watergum Trl",
                City = "El Cajon",
                State = "CA",
                Postcode = "92071",
                TotalBeds = 6,
                TotalBath = 5,
                TotalSquareFeet = 3798,
                Represented = "Buyer",
                SoldDate = new DateTime(2017, 3, 4)
            };
            lstAgentPastSale.Add(agentPastSaleModel2);

            AgentPastSaleModel agentPastSaleModel3 = new AgentPastSaleModel
            {
                SaleId = 3,
                PropertyImage = "home12.jpg",
                PropertyPrice = 688000,
                PropertyAddress = "627 Hawthorne Ave",
                City = "El Cajon",
                State = "CA",
                Postcode = "92071",
                TotalBeds = 3,
                TotalBath = 3,
                TotalSquareFeet = 4798,
                Represented = "Buyer",
                SoldDate = new DateTime(2018, 1, 1)
            };
            lstAgentPastSale.Add(agentPastSaleModel3);

            AgentPastSaleModel agentPastSaleModel4 = new AgentPastSaleModel
            {
                SaleId = 4,
                PropertyImage = "home6.jpg",
                PropertyPrice = 988000,
                PropertyAddress = "9565 Janfred Way",
                City = "La Mesa",
                State = "CA",
                Postcode = "91942",
                TotalBeds = 5,
                TotalBath = 5,
                TotalSquareFeet = 2000,
                Represented = "Seller",
                SoldDate = new DateTime(2017, 10, 12)
            };
            lstAgentPastSale.Add(agentPastSaleModel4);

            AgentPastSaleModel agentPastSaleModel5 = new AgentPastSaleModel
            {
                SaleId = 5,
                PropertyImage = "home7.jpg",
                PropertyPrice = 688000,
                PropertyAddress = "555 Hawthorne Ave",
                City = "El Cajon",
                State = "CA",
                Postcode = "92071",
                TotalBeds = 3,
                TotalBath = 3,
                TotalSquareFeet = 4567,
                Represented = "Seller",
                SoldDate = new DateTime(2018, 1, 1)
            };
            lstAgentPastSale.Add(agentPastSaleModel5);

            return lstAgentPastSale.OrderByDescending(x => x.SoldDate).ToList();
        }

        private List<AgentReviewModel> GetAgentReview()
        {
            List<AgentReviewModel> lstAgentReview = new List<AgentReviewModel>();

            AgentReviewModel agentReviewModel1 = new AgentReviewModel
            {
                ReviewId = 1,
                AgentRating = (decimal)1.5,
                LocalKnowledgeRating = (decimal)3.6,
                ProcessExpertiseRating = (decimal)2.4,
                ResponsivenessRating = (decimal)4.2,
                NegotiationSkillsRating = (decimal)4.6,
                ReviewDate = new DateTime(2016, 2, 3),
                ReviewDescription = "Being a full-service Realtor since 2007, I have been baptized by fire in a very tough housing market. I have successfully closed over 60 transactions and processed over 70 short sales both as the listing agent and some for other agents. I am very knowledgeable about lenders and their processes. I strive to exceed expectations and never forget that I am always accountable to my clients.",
                ReviewedBy = "russrobertsi",
                ReviewTag = "Bought a home in 2016 in La Mesa, CA"
            };
            lstAgentReview.Add(agentReviewModel1);

            AgentReviewModel agentReviewModel2 = new AgentReviewModel
            {
                ReviewId = 2,
                AgentRating = (decimal)3.5,
                LocalKnowledgeRating = (decimal)2.6,
                ProcessExpertiseRating = (decimal)3.4,
                ResponsivenessRating = (decimal)4.8,
                NegotiationSkillsRating = (decimal)4.6,
                ReviewDate = new DateTime(2017, 3, 4),
                ReviewDescription = "Most important to me was communication and Mark saw that every question or concern of ours we met with full and complete information. In most cases, Mark delivered information to us before we even had to ask. I look forward to working with Mark in the future because I know that I can trust him to",
                ReviewedBy = "Jayesh Jogani",
                ReviewTag = "Bought a home in 2017 in El Cajon, CA"
            };
            lstAgentReview.Add(agentReviewModel2);

            return lstAgentReview.OrderByDescending(x => x.ReviewDate).ToList();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
