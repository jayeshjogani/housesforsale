﻿
namespace Houses.ForSale.Models
{
    public class AgentOverviewModel
    {
        public int AgentId { get; set; }
        public string AgentFirstName { get; set; }
        public string AgentLastName { get; set; }
        public string AgentPhoto { get; set; }
        public decimal AgentRating { get; set; }
        public decimal LocalKnowledgeRating { get; set; }
        public decimal ProcessExpertiseRating { get; set; }
        public decimal ResponsivenessRating { get; set; }
        public decimal NegotiationSkillsRating { get; set; }
        public int AgentReviews { get; set; }
        public string AgentDescription { get; set; }
        public string AgentObjective { get; set; }
        public string AgentBrokerage { get; set; }
        public string AgentSpecialties { get; set; }
        public string AgentLicenseNumber { get; set; }
    }
}
