﻿
namespace Houses.ForSale.Models
{
    public class AgentActiveListingModel
    {
        public int ListingId { get; set; }
        public bool IsFavorite { get; set; }
        public string PropertyImage { get; set; }
        public int PropertyPrice { get; set; }
        public bool? IsInstallment { get; set; }
        public int? InstallmentPrice { get; set; }
        public string PropertyAddress { get; set; }
        public int TotalBeds { get; set; }
        public int TotalBath { get; set; }
        public int TotalSquareFeet { get; set; }
    }
}
