﻿using System.Collections.Generic;

namespace Houses.ForSale.Models
{
    public class AgentProfileModel
    {
        public AgentOverviewModel agentOverview { get; set; }
        public List<AgentActiveListingModel> lstAgentActiveListing { get; set; }
        public List<AgentPastSaleModel> lstAgentPastSale { get; set; }
        public List<AgentReviewModel> lstAgentReview { get; set; }
    }
}
