﻿using System;

namespace Houses.ForSale.Models
{
    public class AgentPastSaleModel
    {
        public int SaleId { get; set; }
        public string PropertyImage { get; set; }
        public int PropertyPrice { get; set; }
        public string PropertyAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public int TotalBeds { get; set; }
        public int TotalBath { get; set; }
        public int TotalSquareFeet { get; set; }
        public DateTime SoldDate { get; set; }
        public string Represented { get; set; }
    }
}
