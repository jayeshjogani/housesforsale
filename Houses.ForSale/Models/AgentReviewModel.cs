﻿using System;

namespace Houses.ForSale.Models
{
    public class AgentReviewModel
    {
        public int ReviewId { get; set; }
        public DateTime ReviewDate { get; set; }
        public decimal AgentRating { get; set; }
        public decimal LocalKnowledgeRating { get; set; }
        public decimal ProcessExpertiseRating { get; set; }
        public decimal ResponsivenessRating { get; set; }
        public decimal NegotiationSkillsRating { get; set; }
        public string ReviewedBy { get; set; }
        public string ReviewTag { get; set; }
        public string ReviewDescription { get; set; }
    }
}
